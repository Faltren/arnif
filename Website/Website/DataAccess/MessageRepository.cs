﻿using AutoMapper;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Website.DataAccess.Interfaces;

namespace Website.DataAccess.EFModels
{
    public class MessageRepository : Repository<EFModels.Messages, Dbo.Message>, IMessageRepository
    {
        public MessageRepository(arnifContext context, IMapper mapper) : base(context, mapper)
        {
        }
    }
}
