﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Website.DataAccess.Interfaces
{
    public interface IMessageRepository : IRepository<EFModels.Messages, Dbo.Message>
    {
    }
}
