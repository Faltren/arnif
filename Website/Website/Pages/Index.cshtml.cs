﻿using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Website.DataAccess.Interfaces;
using Website.Dbo;

namespace Website.Pages
{
    public class IndexModel : PageModel
    {
        [BindProperty(SupportsGet = true)]
        [Required]
        public string message { get; set; }

        [BindProperty(SupportsGet = true)]
        [Required]
        public string username { get; set; }

        private readonly IMessageRepository messageRepository_;
        public List<Message> Messages;

        public IndexModel(IMessageRepository messageRepository)
        {
            messageRepository_ = messageRepository;
            Messages = new List<Message>();

            message = "";
            username = "";
        }

        public async Task OnGet()
        {
            Messages = (await messageRepository_.Get()).ToList();
        }

        public async Task<IActionResult> OnPost()
        {
            Message message = new Message()
            {
                Body = this.message,
                Username = this.username + " le " + DateTime.Now.ToString("dddd, dd MMMM yyyy HH:mm:ss")
            };

            await messageRepository_.Insert(message);

            return Redirect(Request.GetDisplayUrl());
        }
    }
}
