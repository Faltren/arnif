﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Website.Dbo
{
    public interface IObjectWithId
    {
        int Id { get; set; }
    }
}
