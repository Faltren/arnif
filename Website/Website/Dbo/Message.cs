﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Website.Dbo
{
    public class Message : IObjectWithId
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Body { get; set; }
    }
}
