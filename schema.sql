DROP TABLE IF EXISTS messages;
CREATE TABLE IF NOT EXISTS messages(
  id serial PRIMARY KEY,
  username VARCHAR(255),
  body TEXT
);
